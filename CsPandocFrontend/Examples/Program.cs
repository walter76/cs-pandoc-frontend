﻿using CsPandocFrontend;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examples
{
    class Program
    {
        static void Main(string[] args)
        {
            var markdown = File.ReadAllText(@"C:\home\workspace\src\cs-pandoc-frontend\entropy\table.md");
            var pandoc = new Pandoc();
            pandoc.Execute(markdown, @"C:\home\workspace\src\cs-pandoc-frontend\entropy\table.docx");
        }
    }
}
