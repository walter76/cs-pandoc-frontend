﻿using System.Diagnostics;

namespace CsPandocFrontend
{
    public class Pandoc
    {
        public void Execute(string markdown, string filename)
        {
            var processStartInfo = new ProcessStartInfo
            {
                UseShellExecute = false,
                RedirectStandardInput = true,
                FileName = @"pandoc",
                Arguments = $"-f gfm -t docx -o {filename}"
            };

            var process = new Process
            {
                StartInfo = processStartInfo
            };
            process.Start();

            process.StandardInput.WriteLine(markdown);
            process.StandardInput.Close();

            process.WaitForExit(60000);
        }
    }
}
